import { async, fakeAsync, TestBed, ComponentFixture, tick } from '@angular/core/testing';

import { VerifyHandsService } from './verify-hands.service';
import { HandsComponent } from '../hands/hands.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('VerifyHandsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [VerifyHandsService]
    })
  });

  it('should be created', () => {
    const service: VerifyHandsService = TestBed.get(VerifyHandsService);
    expect(service).toBeTruthy();
  });
});
