import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

const baseURL = 'http://localhost:3000/poker';

@Injectable({
  providedIn: 'root'
})
export class VerifyHandsService {

  constructor(private http: HttpClient) { }

  verifyHands(hands: Array<string>) {    
    const params = new HttpParams().set('hands', hands.map(hand => hand.toString().replace(/,/g,"")).toString());

    return this.http.request('get', baseURL, { params: params });
  }
}
