import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HandsComponent } from './hands/hands.component';

export const ROUTES: any = [
  { path: 'hands', text: 'Hands', component: HandsComponent },
  {
    path: '',
    redirectTo: '/hands',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
