import { Component, OnInit } from '@angular/core';
import { VerifyHandsService } from '../services/verify-hands.service';

@Component({
  selector: 'app-hands',
  templateUrl: './hands.component.html',
  styleUrls: ['./hands.component.scss']
})
export class HandsComponent implements OnInit {
  public players: Array<any> = [];
  public winnerPlayer = {};
  public errorMsg = "";
  public showWinner = false;
  public suits = ["C", "O", "P", "E"];
  public kinds = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"];
  public cards = [];
  public totalCards = 52;
  constructor(private verifyHandsService: VerifyHandsService) { }

  ngOnInit() {
    
  }

  createCardsDeck() {
    this.suits.map(suit => {
      this.kinds.map(kind => {
        this.cards.push(kind + suit);
      });
    });
  }

  addPlayer() {
    if (this.players.length == 10) {
      this.errorMsg = "Número máximo de jogadores foi atingido!";

      setTimeout(() => {
        this.errorMsg = "";
      }, 3000);
    } else {
      this.players.push({
        cards: []
      });

      this.clearHands();
    }
  }

  clearPlayers() {
    this.players = [];
  }

  generateHands() {
    this.clearHands();

    this.players.map(player => {
      for (let i = 0; i < 5; i++) {
        player.cards.push(this.randomCards())
      }
    });
  }

  clearHands() {
    this.players.map(player => {
      player.cards = [];
    });
    this.showWinner = false;
    this.winnerPlayer = {};
    this.totalCards = 52;
    this.createCardsDeck();
  }

  randomCards() {
    const randomNumber = Math.floor(Math.random() * this.totalCards--) + 1;
    const generatedCard = this.cards[randomNumber];
    if (!generatedCard) {
      return this.randomCards();
    }
    this.cards.splice(randomNumber, 1);

    return generatedCard;
  }

  verifyHands() {
    this.verifyHandsService.verifyHands(this.players.map(hands => hands.cards))
      .subscribe((results: any) => {
        this.showWinner = true;
        this.winnerPlayer = results;
      });
  }

}
