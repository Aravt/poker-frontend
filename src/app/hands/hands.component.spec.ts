import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { HandsComponent } from './hands.component';
import { VerifyHandsService } from '../services/verify-hands.service';

describe('HandsComponent', () => {
  let component: HandsComponent;
  let fixture: ComponentFixture<HandsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      declarations: [ HandsComponent ],
      providers: [ VerifyHandsService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should have as title 'Hands'`, () => {
    const fixture = TestBed.createComponent(HandsComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Hands');
  });

  it(`should have a button with 'Add player' as text`, () => {
    const fixture = TestBed.createComponent(HandsComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.addPlayerBtn').textContent).toContain('Add player');
  });

  it(`should have a button with 'Clear players' as text`, () => {
    const fixture = TestBed.createComponent(HandsComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.clearPlayersBtn').textContent).toContain('Clear players');
  });

  it(`should have a button with 'Generate hands' as text`, () => {
    const fixture = TestBed.createComponent(HandsComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.generateHandBtn').textContent).toContain('Generate hands');
  });

  it(`should have a button with 'Verify hands' as text`, () => {
    const fixture = TestBed.createComponent(HandsComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.verifyHandsBtn').textContent).toContain('Verify hands');
  });

  it('should add players to a players list', async(() => {
    let players = [];
    spyOn(component, 'addPlayer');
  
    let button = fixture.debugElement.nativeElement.querySelector('.addPlayerBtn');
    button.click();
  
    fixture.whenStable().then(() => {
      expect(component.addPlayer).toHaveBeenCalled();
      players.push({});
    }).then(() => {
      expect(players.length).toBe(1);
    });
  }));

  it('should clear players list', async(() => {
    let players = [{},{},{},{}];
    spyOn(component, 'clearPlayers');
  
    let button = fixture.debugElement.nativeElement.querySelector('.clearPlayersBtn');
    button.click();
  
    fixture.whenStable().then(() => {
      expect(component.clearPlayers).toHaveBeenCalled();
      players = [];
    }).then(() => {
      expect(players.length).toBe(0);
    });
  }));

  it('should generate a card hand to players from the list', async(() => {
    let players = [{}];
    spyOn(component, 'generateHands');
  
    let button = fixture.debugElement.nativeElement.querySelector('.generateHandBtn');
    button.click();
  
    fixture.whenStable().then(() => {
      expect(component.generateHands).toHaveBeenCalled();
      players[0] = ["2C", "3C", "4C", "5C", "6C"];
    }).then(() => {
      expect(players[0].toString()).toBe(["2C", "3C", "4C", "5C", "6C"].toString());
    });
  }));

  it('should call verifyHands to verify players hands', async(() => {
    const service: VerifyHandsService = TestBed.get(VerifyHandsService);
    const hands = ["6P9CQP5C2C","KE5C5E8C7E","4P3EQEQOAO"];
    spyOn(component, 'verifyHands');
  
    let button = fixture.debugElement.nativeElement.querySelector('.verifyHandsBtn');
    button.click();
  
    fixture.whenStable().then(() => {
      expect(component.verifyHands).toHaveBeenCalled();
      service.verifyHands(hands).subscribe(result => {
        expect(result).toBe({
          player: 2,
          highestCard: 'AO',
          cardsResult: "Two pairs"
        });
      });
    });
  }));
});
